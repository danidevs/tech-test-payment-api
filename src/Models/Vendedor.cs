using System.Collections.Generic;

namespace src.Models;

public class Vendedor
{
    public Vendedor(){
        this.Nome = "template";
        this.contratos = new List<Contrato>();
        this.Ativo = true;
        
    }
    public Vendedor(string Nome, string Cpf, string Email, int Telefone, bool Ativo){
        this.Nome = Nome;
        this.Cpf = Cpf;
        this.Email = Email;
        this.Telefone = Telefone;
        this.Ativo = true;
        this.contratos = new List<Contrato>();
    }
    public int Id { get; set; }
    public string? Nome { get; set; }
    public string? Cpf { get; set; }
    public string? Email { get; set; }
    public int Telefone { get; set; }
    public bool Ativo { get; set; }
    public List<Contrato> contratos { get; set; }

}