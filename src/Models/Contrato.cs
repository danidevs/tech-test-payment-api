namespace src.Models;

public class Contrato
{
    public Contrato()
    {
        this.DataCriacao = DateTime.Now;
        this.valor = 0;
        this.TokenId = "0000000";
        this.Pago = false;
    }

    public Contrato(string TokenID, double valor)
    {
        this.DataCriacao = DateTime.Now;
        this.TokenId = TokenID;
        this.valor = valor;
         this.Pago = false;
    }

 public DateTime DataCriacao   { get; set; }
 public string TokenId { get; set; }
 public double valor { get; set; }  
 public bool Pago { get; set; } 
}