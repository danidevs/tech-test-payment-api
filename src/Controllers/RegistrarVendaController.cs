using Microsoft.AspNetCore.Mvc;
using src.Models;

namespace src.Controllers;

[ApiController]
[Route("[controller]")]
public class RegistrarVendaController : ControllerBase
{
    [HttpGet]
    public Vendedor Get(){
        Vendedor vendedor = new Vendedor();
        Contrato novocontrato = new Contrato("abc123", 50.46);
        vendedor.contratos.Add(novocontrato);
        
        return vendedor;
    }
    [HttpPost]
    public Vendedor post([FromBody]Vendedor vendedor)
    {
        return vendedor;
    }
    [HttpPut("{id}")]
    public string Update ([FromRoute] int id, [FromBody]Vendedor vendedor)
    {
        Console.WriteLine(id);
        Console.WriteLine(vendedor);
        return "Dados do id " +  id + " atualizados";
    }
}